const express = require('express');
const fileUpload = require('express-fileupload');
const Jimp = require('jimp');
const app = express();

/*
async function main() {
  var p1 = Jimp.read('./uploads/t1.jpg');
  var p2 = Jimp.read("./uploads/eurobeat1.png");
  Promise.all([p1, p2]).then(images => {
    images[1].resize(images[0].bitmap.width, Jimp.AUTO);
    let color = getRandomArbitrary(-360, 360);
    images[0].color([
      {
        apply: 'hue',
        params: [color]
      }, {
        apply: 'saturate',
        params: [30]
      }
    ]);
    images[0].composite(images[1], 0, 0).write("output.png");
  });
}
main();
*/

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

app.use('/form', express.static(__dirname + '/index.html'));

// default options
app.use(fileUpload({
  limits: {
    fileSize: 50 * 1024 * 1024
  }
}));

app.get('/ping', function(req, res) {
  res.send('pong');
});

app.post('/upload', function(req, res) {
  let sampleFile;
  let uploadPath;

  if (Object.keys(req.files).length == 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile;
  uploadPath = __dirname + '/uploads/' + sampleFile.name;

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    console.log('p1 ok');
    console.log(sampleFile);

    var p1 = Jimp.read('./uploads/' + sampleFile.name);
    var p2 = Jimp.read("./uploads/eurobeat1.png");

    Promise.all([p1, p2]).then(images => {
      images[1].resize(images[0].bitmap.width, Jimp.AUTO);
      let color = getRandomArbitrary(-360, 360);
      images[0].color([
        {
          apply: 'hue',
          params: [color]
        }, {
          apply: 'saturate',
          params: [30]
        }
      ]);

      console.log('salida lista');
      images[0].composite(images[1], 0, 0).write("output.png", function() {

        res.sendFile(__dirname + '/output.png');
      })

    });

  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
